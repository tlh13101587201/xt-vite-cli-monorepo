import { defineConfig } from "vite";
import { resolve } from "path";

export default defineConfig(({ command, mode, ssrBuild }) => {
  console.log(command, mode, ssrBuild)

  return {
    resolve: {
      alias: {
        "@": resolve(__dirname, "./src"),
      },
    },

    server: {
      port: 8888
    },
  }
})