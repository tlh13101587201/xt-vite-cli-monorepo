/* eslint-env node */

module.exports = {
  root: true,
  extends: [
    "plugin:vue/vue3-essential",
    "eslint:recommended",
    "@vue/eslint-config-typescript",
  ],
  rules: {
    semi: ["error", "always"], // 是否使用结尾分号
    quotes: ["error", "double"], // 是否使用单引号，single 单引号，double 双引号
  },
};
