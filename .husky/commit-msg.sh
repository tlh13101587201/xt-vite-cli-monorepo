#!/bin/bash
MSG=`awk '{printf("%s",$0)}' $1`
echo -e $MSG
# if [[ $MSG =~ ^((feat|fix|test|refactor|docs|style|chore|build):.*((\-\-bug=[0-9]+ \-\-bug\-reason=.*)|(\-\-dev=)))|(Split|Merge).*$ ]]
if [[ $MSG =~ ^((feat|fix|test|refactor|docs|style|chore|build): ((\-\-bug=[0-9]+ \-\-bug\-reason=.*)|(\-\-dev=)))|(Split|Merge).*$ ]]
then
	echo -e "\033[32m commit success! \033[0m"
else
    echo -e "\033[31m Error: the commit message is error\033[m"
	echo -e "\033[31m Error: type must be one of [feat,fix,test,refactor,docs,style,chore,build] \033[m"
    echo -e "\033[31m eg: Your subject should contain feat: (userName) --bug=id --bug-reason=message or feat: --dev=id message \033[m"
	exit 1
fi