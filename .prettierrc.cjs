/**
 * prettier vscode 中保存自动生效
 * https://blog.csdn.net/weixin_62277266/article/details/124227763
 *
 * prettier 配置参考
 * https://www.jianshu.com/p/3dd65358c7e5
 */
module.exports = {
  tabWidth: 2, // 使用 2 个空格进行缩进
  singleQuote: false, // 是否使用单引号
  semi: true, // 是否使用分号结尾
  trailingComma: "all", // 是否使用尾部逗号 none/es5/all
};
